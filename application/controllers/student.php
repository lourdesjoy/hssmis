<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class student extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

	}

	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('student/info');
		$this->load->view('template/footer');
	}


	public function viewsubject()
	{
		$this->load->view('template/header');
		$this->load->view('student/subject');
		$this->load->view('template/header');
	}

	
}
